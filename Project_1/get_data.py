"""It's module for getting parameters from the house"""
import requests
import random
#import urllib3


def get_temperature():
    #r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=London&{YOUR_API_KEY}')
    #I have not API key, so:
    try:
        resp_temperature = requests.get('https://httpmytrainhouse.house/temperature')
    except requests.exceptions.ConnectionError as e:
        print(f'{e} - it\'s OK')
        if hasattr(get_temperature, 'last_temp'):
            if 12.0 < get_temperature.last_temp < 25.0:
                get_temperature.last_temp = round((get_temperature.last_temp + random.uniform(-1, 1)), 1)
        else:
            get_temperature.last_temp = 18.0
        resp_temperature = get_temperature.last_temp
    finally:
        return resp_temperature


def get_lightning_sensor():
    try:
        resp_lightning_sensor = requests.get('https://httpmytrainhouse.house/lightning_sensor')
    except requests.exceptions.ConnectionError as e:
        print(f'{e} - it\'s OK')
        resp_lightning_sensor = True
    finally:
        return resp_lightning_sensor


def get_security_sensors():
    try:
        resp_security_sensors = requests.get('https://httpmytrainhouse.house/securitysensors')
    except requests.exceptions.ConnectionError as e:
        print(f'{e} - it\'s OK')
        resp_security_sensors = {'Датчик двери №1': [0, 0, 0, 0, 0, 0, 0, 0, 1][random.randint(0, 8)],
                'Датчик двери №2': [0, 0, 0, 0, 0, 0, 0, 0, 1][random.randint(0, 8)],
                'Датчик двери №3': [0, 0, 0, 0, 0, 0, 0, 0, 1][random.randint(0, 8)],
                'Датчик движения №1': [0, 0, 0, 0, 0, 0, 0, 0, 1][random.randint(0, 8)],
                'Датчик движения №2': [0, 0, 0, 0, 0, 0, 0, 0, 1][random.randint(0, 8)],
                'Датчик движения №3': [0, 0, 0, 0, 0, 0, 0, 0, 1][random.randint(0, 8)],
                'Датчик шума №1': [0, 0, 0, 0, 0, 0, 0, 0, 1][random.randint(0, 8)],
                'Датчик шума №2': [0, 0, 0, 0, 0, 0, 0, 0, 1][random.randint(0, 8)]}
    finally:
        return resp_security_sensors


def get_fire_sensors():
    try:
        resp_fire_sensors = requests.get('https://httpmytrainhouse.house/firesensors')
    except requests.exceptions.ConnectionError as e:
        print(f'{e} - it\'s OK')
        resp_fire_sensors = {'Датчик задымления №1': [0, 0, 0, 0, 1][random.randint(0, 4)],
                'Датчик задымления №2': [0, 0, 0, 0, 1][random.randint(0, 4)],
                'Датчик задымления №3': [0, 0, 0, 0, 1][random.randint(0, 4)]}
    finally:
        return resp_fire_sensors
