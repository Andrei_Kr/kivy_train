"""It's module for putting parameters to the systems of house"""
import requests
import urllib3


def sent_contactor(ip_address, required_state):
    try:
        r = requests.put('http://ip_address/get/m2m/outputs?type=rele&number=0&hash=HASH')
        '''
        Answer must looks like:
        {"outputs":{"type":"rele","total_numbers":2,"data":[{"number":0,"value":1,"error_ch":0},\
        {"number":1,"value":0,"error_ch":0}]},"error_device":0}
        '''
        if r[72] is not str(required_state):
            r = requests.put(f'http://{ip_address}/set/m2m/outputs?type=rele&number=0&value={required_state}'
                             f'&hash=HASH')
            if r[46] is not required_state:
                raise RuntimeError(f"Contactor {ip_address} doesn't work")
    except urllib3.exceptions.NewConnectionError as e:
        print(f'{e} - it\'s OK')


def sent_video(list_of_methods_of_control_videocamers, list_of_status_of_videocamers):
    pass



