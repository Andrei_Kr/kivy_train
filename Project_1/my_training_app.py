import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.recycleview import RecycleView
from kivy.core.window import Window
from datetime import datetime
from kivy.metrics import dp
import ast
from kivy.uix.gridlayout import GridLayout
from kivy.config import ConfigParser
import os
import ast
import time
from kivy.uix.checkbox import CheckBox
from kivy.uix.layout import Layout
from kivy.uix.textinput import TextInput
from kivy.uix.switch import Switch
from kivy.uix.popup import Popup
from kivy.lang import Builder
from kivy.clock import Clock


import get_data
import sent_data
import controller


class HomeSystems(Screen):
    def __init__(self, **kw):
        super(HomeSystems, self).__init__(**kw)
        box_1 = BoxLayout(orientation='vertical', padding=5)
        box_1.add_widget(Label(text='Системы', font_size=40, color=(0, 0, 1, 1), size_hint=(1, .2)))
        box_2 = BoxLayout(orientation='vertical', size_hint=(1, .8))
        #self.manager.transition.direction = 'left'
        box_2.add_widget(Button(text='Отопление', font_size=25, background_color=(0, 1, 0, 1), color=(1, 0, 1, 1),
                                on_press=lambda x: set_screen('heater')))
        box_2.add_widget(Button(text='Наружное освещение', font_size=25, background_color=(0, 1, 0, 1),
                                color=(0, 1, 1, 1), on_press=lambda x: set_screen('out_lightning')))
        box_2.add_widget(Button(text='Бытовые приборы', font_size=25, background_color=(0, 1, 0, 1), color=(3, 1, 1, 1),
                                on_press=lambda x: set_screen('home_devices')))
        box_2.add_widget(Button(text='Охранная сигнализация', font_size=25, background_color=(0, 1, 0, 1),
                                color=(1, 0, 0, 1), on_press=lambda x: set_screen('security_alarm')))
        box_2.add_widget(Button(text='Пожарная сигнализация', font_size=25, background_color=(0, 1, 0, 1),
                                color=(1, 0.4, 0, 1), on_press=lambda x: set_screen('fire_alarm')))
        box_2.add_widget(Button(text='Видеонаблюдение', font_size=25, background_color=(0, 1, 0, 1),
                                color=(0.8, 0.4, 1, 1), on_press=lambda x: set_screen('video')))
        box_1.add_widget(box_2)
        self.add_widget(box_1)
        Clock.schedule_once(lambda x: controller.control_temperature())
        Clock.schedule_once(lambda x: controller.control_outlightning())
        Clock.schedule_once(lambda x: controller.control_homedevices())
        Clock.schedule_once(lambda x: controller.control_securityalarm())
        Clock.schedule_once(lambda x: controller.control_firealarm())
        Clock.schedule_once(lambda x: controller.control_video())
        Clock.schedule_interval(lambda x: controller.control_temperature(), 180)
        Clock.schedule_interval(lambda x: controller.control_outlightning(), 600)
        Clock.schedule_interval(lambda x: controller.control_homedevices(), 60)
        Clock.schedule_interval(lambda x: controller.control_securityalarm(), 5)
        Clock.schedule_interval(lambda x: controller.control_firealarm(), 5)
        Clock.schedule_interval(lambda x: controller.control_video(), 5)


class Heater(Screen):
    def __init__(self, **kw):
        self.text_in_box_3 = TextInput(text='18.0', font_size=45, size_hint=(.15, 1))
        self.checkbox_1 = CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=False,
                                   on_press=lambda x: self.label_change())
        self.switch_1 = Switch(active=False, size_hint=(1, .1))
        super(Heater, self).__init__(**kw)

    def on_enter(self):  # Будет вызвана в момент открытия экрана
        self.manager.transition.direction = 'right'
        box_1 = BoxLayout(orientation='vertical', spacing=15, padding=5)
        box_2 = BoxLayout(orientation='horizontal', size_hint=(1, .2))
        box_2.add_widget(Button(text='< Назад', on_press=lambda x: set_screen('menu'), size_hint=(.15, 1)))
        box_2.add_widget(Label(text='Отопление', font_size=40, color=(1, 0, 1, 1), size_hint=(.85, 1)))
        box_1.add_widget(box_2)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=2, color=(2, 1, 1, 2)))
        box_3 = BoxLayout(orientation='horizontal', size_hint=(1, .15))
        box_3.add_widget(Label(text='Требуемая температура:', size_hint=(.55, 1), font_size=30,
                               color=(2, 1, 1, 2)))
        self.text_in_box_3 = TextInput(text=self.text_in_box_3.text, font_size=45, size_hint=(.17, 1))
        box_3.add_widget(self.text_in_box_3)
        box_3.add_widget(Label(text=u'\N{DEGREE SIGN}С', size_hint=(.08, 1), font_size=30, color=(2, 1, 1, 2)))
        btn = Button(text='Подтвердить', size_hint=(.2, 1), font_size=20)
        btn.bind(on_press=self.attach_parameters)
        box_3.add_widget(btn)
        box_1.add_widget(box_3)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=25, color=(2, 1, 1, 2)))
        box_4 = BoxLayout(orientation='horizontal', size_hint=(1, .2))
        box_4.add_widget(Label(text='Ручное управление ', size_hint=(.6, 1), font_size=30, color=(2, 1, 1, 2)))
        self.checkbox_1 = CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=self.checkbox_1.active,
                                   on_press=lambda x: self.label_change())
        box_4.add_widget(self.checkbox_1)
        box_1.add_widget(box_4)
        self.switch_1 = Switch(active=self.switch_1.active, size_hint=(1, .1), disabled=not self.checkbox_1.active)
        self.switch_1.bind(active=self.attach_parameters)
        box_5 = self.switch_1
        box_1.add_widget(box_5)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=25, color=(2, 1, 1, 2)))
        box_6 = BoxLayout(orientation='horizontal', size_hint=(1, .15))
        box_6.add_widget(Label(text='Действующая температура:', size_hint=(.7, 1), font_size=30, color=(1, 0, 1, 1)))
        box_6.add_widget(Label(text=str(controller.home_parameters['temp']), size_hint=(.15, 1), font_size=45,
                               color=(1, 0, 1, 1)))
        box_6.add_widget(Label(text=u'\N{DEGREE SIGN}С', size_hint=(.15, 1), font_size=30, color=(1, 0, 1, 1)))
        box_1.add_widget(box_6)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=25, color=(2, 1, 1, 2)))
        self.add_widget(box_1)

    def on_leave(self):  # Будет вызвана в момент закрытия экрана
        self.attach_parameters()
        self.clear_widgets()  # очищаем список
        self.manager.transition.direction = 'left'

    def label_change(self):  # обновляет экран
        self.attach_parameters()
        self.clear_widgets()
        self.on_enter()

    def attach_parameters(self, *args):  # обновляет параметры в controller.py
        controller.user_parameters['heater']['desired_temperature'] = self.text_in_box_3.text
        controller.user_parameters['heater']['manual_control'] = self.checkbox_1.active
        controller.user_parameters['heater']['on_off_heater'] = self.switch_1.active
        Clock.schedule_once(lambda x: controller.control_temperature())


class OutLightning(Screen):
    def __init__(self, **kw):
        self.list_of_checkboxes = [CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=True,
                                            on_press=lambda x: self.label_change(0)),
                                   CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=False,
                                            on_press=lambda x: self.label_change(1)),
                                   CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=False,
                                            on_press=lambda x: self.label_change(0))]
        self.switch_1 = Switch(active=False)
        self.text_in_box_3_1 = TextInput(text='21:00', font_size=20)
        self.text_in_box_3_2 = TextInput(text='06:00', font_size=20)
        super(OutLightning, self).__init__(**kw)

    def on_enter(self):  # Будет вызвана в момент открытия экрана
        self.manager.transition.direction = 'right'
        box_1 = BoxLayout(orientation='vertical', spacing=15, padding=5)
        box_2 = BoxLayout(orientation='horizontal', size_hint=(1, .2))
        box_2.add_widget(Button(text='< Назад', on_press=lambda x: set_screen('menu'), size_hint=(.15, 1)))
        box_2.add_widget(Label(text='Наружное освещение', font_size=40, color=(0, 4, 1, 1), size_hint=(.85, 1)))
        box_1.add_widget(box_2)
        box_1.add_widget(Label(text=' ', size_hint=(1, .08), font_size=2, color=(2, 1, 1, 2)))
        box_3 = GridLayout(size_hint=(1, .7), rows=3, cols=3)
        box_3.add_widget(Label(text='По датчику', font_size=20, size_hint=(1, .25), color=(0, 4, 1, 1)))
        box_3.add_widget(Label(text='Установить время', font_size=20, size_hint=(1, .25), color=(0, 4, 1, 1)))
        box_3.add_widget(Label(text='Вручную', font_size=20, size_hint=(1, .25), color=(0, 4, 1, 1)))
        self.list_of_checkboxes[0] = CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1),
                                              active=self.list_of_checkboxes[0].active,
                                              on_press=lambda x: self.label_change(0))
        box_3.add_widget(self.list_of_checkboxes[0])
        self.list_of_checkboxes[1] = CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1),
                                              active=self.list_of_checkboxes[1].active,
                                              on_press=lambda x: self.label_change(1))
        box_3.add_widget(self.list_of_checkboxes[1])
        self.list_of_checkboxes[2] = CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1),
                                              active=self.list_of_checkboxes[2].active,
                                              on_press=lambda x: self.label_change(2))
        box_3.add_widget(self.list_of_checkboxes[2])
        box_3.add_widget(Label(text=' '))
        box_3_1 = GridLayout(rows=2, cols=2, size_hint=(1, 0.65))
        box_3_1.add_widget(Label(text='Вкл:', font_size=20, color=(0, 4, 1, 1)))
        self.text_in_box_3_1 = TextInput(text=self.text_in_box_3_1.text, font_size=20)
        box_3_1.add_widget(self.text_in_box_3_1)
        box_3_1.add_widget(Label(text='Выкл:', font_size=20, color=(0, 4, 1, 1)))
        self.text_in_box_3_2 = TextInput(text=self.text_in_box_3_2.text, font_size=20)
        box_3_1.add_widget(self.text_in_box_3_2)
        box_3_1_1 = GridLayout(rows=2, cols=1, disabled=not self.list_of_checkboxes[1].active)
        box_3_1_1.add_widget(box_3_1)
        btn = Button(text='Подтвердить', size_hint=(1, 0.35), font_size=20)
        btn.bind(on_press=self.attach_parameters)
        box_3_1_1.add_widget(btn)
        box_3.add_widget(box_3_1_1)
        self.switch_1 = Switch(active=self.switch_1.active, disabled=not self.list_of_checkboxes[2].active)
        self.switch_1.bind(active=self.attach_parameters)
        box_3.add_widget(self.switch_1)
        box_1.add_widget(box_3)
        box_1.add_widget(Label(text='', font_size=10, size_hint=(1, .02)))
        self.add_widget(box_1)

    def on_leave(self):  # Будет вызвана в момент закрытия экрана
        self.attach_parameters()
        self.clear_widgets()  # очищаем список
        self.manager.transition.direction = 'left'

    def label_change(self, index_of_checkbox):  # обновляет экран, проверка наличия только одной галочки в чекбоксах
        for index, checkbox in enumerate(self.list_of_checkboxes):
            if index != index_of_checkbox:
                checkbox.active = False
            else:
                checkbox.active = True
        self.attach_parameters()
        self.clear_widgets()
        self.on_enter()

    def attach_parameters(self, *args):  # обновляет параметры в controller.py
        index_method_of_control = [x.active for x in self.list_of_checkboxes].index(True)
        controller.user_parameters['outlightning']['method_of_control'] = index_method_of_control
        controller.user_parameters['outlightning']['time_to_on'] = self.text_in_box_3_1.text
        controller.user_parameters['outlightning']['time_to_off'] = self.text_in_box_3_2.text
        controller.user_parameters['outlightning']['on_off_lightning'] = self.switch_1.active
        Clock.schedule_once(lambda x: controller.control_outlightning())


class HomeDevices(Screen):
    def __init__(self, **kw):
        self.list_of_switches = [Switch(active=False), Switch(active=False), Switch(active=False), Switch(active=False),
                                 Switch(active=False), Switch(active=False), Switch(active=False), Switch(active=False)]
        self.list_of_text_inputs_1 = [TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12)]
        self.list_of_text_inputs_2 = [TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12),
                                      TextInput(text='00:00', font_size=20, size_hint=(.3, 1), padding=12)]
        super(HomeDevices, self).__init__(**kw)

    def on_enter(self):  # Будет вызвана в момент открытия экрана
        self.manager.transition.direction = 'right'
        box_1 = BoxLayout(orientation='vertical', spacing=15, padding=5)
        box_2 = BoxLayout(orientation='horizontal', size_hint=(1, .2))
        box_2.add_widget(Button(text='< Назад', on_press=lambda x: set_screen('menu'), size_hint=(.15, 1)))
        box_2.add_widget(Label(text='Бытовые приборы', font_size=40, color=(3, 1, 1, 1), size_hint=(.85, 1)))
        box_1.add_widget(box_2)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=2, color=(2, 1, 1, 2)))
        box_3 = BoxLayout(orientation='horizontal', size_hint=(1, .1))
        box_3.add_widget(Label(text='Прибор', font_size=25, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        box_3.add_widget(Label(text='Вкл/Выкл', font_size=25, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        box_3.add_widget(Label(text='Время вкл.', font_size=25, color=(3, 1, 1, 1), size_hint=(.3, 1)))
        box_3.add_widget(Label(text='Время выкл.', font_size=25, color=(3, 1, 1, 1), size_hint=(.3, 1)))
        box_1.add_widget(box_3)
        box_4 = BoxLayout(orientation='vertical', size_hint_y=None)
        box_4.bind(minimum_height=box_4.setter('height'))
        box_4_1 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
        box_4_1.add_widget(Label(text='Чайник', font_size=20, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        self.list_of_switches[0] = Switch(active=self.list_of_switches[0].active, size_hint=(.2, 1))
        self.list_of_switches[0].bind(active=self.attach_parameters)
        box_4_1.add_widget(self.list_of_switches[0])
        self.list_of_text_inputs_1[0] = TextInput(text=self.list_of_text_inputs_1[0].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_1.add_widget(self.list_of_text_inputs_1[0])
        self.list_of_text_inputs_2[0] = TextInput(text=self.list_of_text_inputs_2[0].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_1.add_widget(self.list_of_text_inputs_2[0])
        box_4.add_widget(box_4_1)
        box_4_2 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
        box_4_2.add_widget(Label(text='Духовка', font_size=20, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        self.list_of_switches[1] = Switch(active=self.list_of_switches[1].active, size_hint=(.2, 1))
        self.list_of_switches[1].bind(active=self.attach_parameters)
        box_4_2.add_widget(self.list_of_switches[1])
        self.list_of_text_inputs_1[1] = TextInput(text=self.list_of_text_inputs_1[1].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_2.add_widget(self.list_of_text_inputs_1[1])
        self.list_of_text_inputs_2[1] = TextInput(text=self.list_of_text_inputs_2[1].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_2.add_widget(self.list_of_text_inputs_2[1])
        box_4.add_widget(box_4_2)
        box_4_3 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
        box_4_3.add_widget(Label(text='Стиральная\nмашина', font_size=20, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        self.list_of_switches[2] = Switch(active=self.list_of_switches[2].active, size_hint=(.2, 1))
        self.list_of_switches[2].bind(active=self.attach_parameters)
        box_4_3.add_widget(self.list_of_switches[2])
        self.list_of_text_inputs_1[2] = TextInput(text=self.list_of_text_inputs_1[2].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_3.add_widget(self.list_of_text_inputs_1[2])
        self.list_of_text_inputs_2[2] = TextInput(text=self.list_of_text_inputs_2[2].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_3.add_widget(self.list_of_text_inputs_2[2])
        box_4.add_widget(box_4_3)
        box_4_4 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
        box_4_4.add_widget(Label(text='Посудомоечная\nмашина', font_size=20, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        self.list_of_switches[3] = Switch(active=self.list_of_switches[3].active, size_hint=(.2, 1))
        self.list_of_switches[3].bind(active=self.attach_parameters)
        box_4_4.add_widget(self.list_of_switches[3])
        self.list_of_text_inputs_1[3] = TextInput(text=self.list_of_text_inputs_1[3].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_4.add_widget(self.list_of_text_inputs_1[3])
        self.list_of_text_inputs_2[3] = TextInput(text=self.list_of_text_inputs_2[3].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_4.add_widget(self.list_of_text_inputs_2[3])
        box_4.add_widget(box_4_4)
        box_4_5 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
        box_4_5.add_widget(Label(text='Мультиварка', font_size=20, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        self.list_of_switches[4] = Switch(active=self.list_of_switches[4].active, size_hint=(.2, 1))
        self.list_of_switches[4].bind(active=self.attach_parameters)
        box_4_5.add_widget(self.list_of_switches[4])
        self.list_of_text_inputs_1[4] = TextInput(text=self.list_of_text_inputs_1[4].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_5.add_widget(self.list_of_text_inputs_1[4])
        self.list_of_text_inputs_2[4] = TextInput(text=self.list_of_text_inputs_2[4].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_5.add_widget(self.list_of_text_inputs_2[4])
        box_4.add_widget(box_4_5)
        box_4_6 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
        box_4_6.add_widget(Label(text='Сушилка', font_size=20, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        self.list_of_switches[5] = Switch(active=self.list_of_switches[5].active, size_hint=(.2, 1))
        self.list_of_switches[5].bind(active=self.attach_parameters)
        box_4_6.add_widget(self.list_of_switches[5])
        self.list_of_text_inputs_1[5] = TextInput(text=self.list_of_text_inputs_1[5].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_6.add_widget(self.list_of_text_inputs_1[5])
        self.list_of_text_inputs_2[5] = TextInput(text=self.list_of_text_inputs_2[5].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_6.add_widget(self.list_of_text_inputs_2[5])
        box_4.add_widget(box_4_6)
        box_4_7 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
        box_4_7.add_widget(Label(text='Вентиляция', font_size=20, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        self.list_of_switches[6] = Switch(active=self.list_of_switches[6].active, size_hint=(.2, 1))
        self.list_of_switches[6].bind(active=self.attach_parameters)
        box_4_7.add_widget(self.list_of_switches[6])
        self.list_of_text_inputs_1[6] = TextInput(text=self.list_of_text_inputs_1[6].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_7.add_widget(self.list_of_text_inputs_1[6])
        self.list_of_text_inputs_2[6] = TextInput(text=self.list_of_text_inputs_2[6].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_7.add_widget(self.list_of_text_inputs_2[6])
        box_4.add_widget(box_4_7)
        box_4_8 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
        box_4_8.add_widget(Label(text='Тостер', font_size=20, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        self.list_of_switches[7] = Switch(active=self.list_of_switches[7].active, size_hint=(.2, 1))
        self.list_of_switches[7].bind(active=self.attach_parameters)
        box_4_8.add_widget(self.list_of_switches[7])
        self.list_of_text_inputs_1[7] = TextInput(text=self.list_of_text_inputs_1[7].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_8.add_widget(self.list_of_text_inputs_1[7])
        self.list_of_text_inputs_2[7] = TextInput(text=self.list_of_text_inputs_2[7].text, font_size=20,
                                                  size_hint=(.3, 1), padding=12)
        box_4_8.add_widget(self.list_of_text_inputs_2[7])
        box_4.add_widget(box_4_8)
        root = RecycleView(size_hint=(1, 0.55))
        root.add_widget(box_4)
        box_1.add_widget(root)
        box_5 = BoxLayout(orientation='horizontal', size_hint=(1, .1))
        box_5.add_widget(Label(text='', font_size=25, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        box_5.add_widget(Label(text='', font_size=25, color=(3, 1, 1, 1), size_hint=(.2, 1)))
        btn = Button(text='Подтвердить', size_hint=(.6, 1), font_size=20)
        btn.bind(on_press=self.attach_parameters)
        box_5.add_widget(btn)
        box_1.add_widget(box_5)
        box_1.add_widget(Label(text='', font_size=10, size_hint=(1, .03)))
        self.add_widget(box_1)

    def on_leave(self):  # Будет вызвана в момент закрытия экрана
        self.attach_parameters()
        self.clear_widgets()  # очищаем список
        self.manager.transition.direction = 'left'

    def attach_parameters(self, *args):  # обновляет параметры в controller.py
        controller.user_parameters['homedevices']['status_of_home_devices'] = [self.list_of_switches[0].active,
                                                                               self.list_of_switches[1].active,
                                                                               self.list_of_switches[2].active,
                                                                               self.list_of_switches[3].active,
                                                                               self.list_of_switches[4].active,
                                                                               self.list_of_switches[5].active,
                                                                               self.list_of_switches[6].active,
                                                                               self.list_of_switches[7].active]
        controller.user_parameters['homedevices']['time_to_on_of_home_devices'] = [self.list_of_text_inputs_1[0].text,
                                                                                   self.list_of_text_inputs_1[1].text,
                                                                                   self.list_of_text_inputs_1[2].text,
                                                                                   self.list_of_text_inputs_1[3].text,
                                                                                   self.list_of_text_inputs_1[4].text,
                                                                                   self.list_of_text_inputs_1[5].text,
                                                                                   self.list_of_text_inputs_1[6].text,
                                                                                   self.list_of_text_inputs_1[7].text]
        controller.user_parameters['homedevices']['time_to_off_of_home_devices'] = [self.list_of_text_inputs_2[0].text,
                                                                                    self.list_of_text_inputs_2[1].text,
                                                                                    self.list_of_text_inputs_2[2].text,
                                                                                    self.list_of_text_inputs_2[3].text,
                                                                                    self.list_of_text_inputs_2[4].text,
                                                                                    self.list_of_text_inputs_2[5].text,
                                                                                    self.list_of_text_inputs_2[6].text,
                                                                                    self.list_of_text_inputs_2[7].text]


class SecurityAlarm(Screen):
    def __init__(self, **kw):
        self.switch_1 = Switch(active=False, size_hint=(.25, 1))
        super(SecurityAlarm, self).__init__(**kw)

    def on_enter(self):  # Будет вызвана в момент открытия экрана
        self.manager.transition.direction = 'right'
        box_1 = BoxLayout(orientation='vertical', spacing=15, padding=5)
        box_2 = BoxLayout(orientation='horizontal', size_hint=(1, .2))
        box_2.add_widget(Button(text='< Назад', on_press=lambda x: set_screen('menu'), size_hint=(.15, 1)))
        box_2.add_widget(Label(text='Охранная сигнализация', font_size=40, color=(1, 0, 0, 1), size_hint=(.85, 1)))
        box_1.add_widget(box_2)
        box_1.add_widget(Label(text='', font_size=25, color=(1, 0, 0, 1), size_hint=(1, .05)))
        box_3 = BoxLayout(orientation='horizontal', size_hint=(1, .15))
        box_3.add_widget(Label(text='Вкл/выкл охранную сигнализацию', font_size=25, color=(1, 0, 0, 1),
                               size_hint=(.7, 1)))
        self.switch_1 = Switch(active=self.switch_1.active, size_hint=(.3, 1))
        self.switch_1.bind(active=self.sensors_checker)
        box_3.add_widget(self.switch_1)
        box_1.add_widget(box_3)
        box_1.add_widget(Label(text='', font_size=25, color=(1, 0, 0, 1), size_hint=(1, .05)))
        box_4 = BoxLayout(orientation='horizontal', size_hint=(1, .15))
        box_4.add_widget(Label(text='Датчик', font_size=25, color=(3, 1, 1, 1), size_hint=(.7, 1)))
        box_4.add_widget(Label(text='Состояние', font_size=25, color=(3, 1, 1, 1), size_hint=(.3, 1)))
        box_1.add_widget(box_4)
        box_5 = BoxLayout(orientation='vertical', size_hint_y=None)
        box_5.bind(minimum_height=box_5.setter('height'))
        for sensor in controller.home_parameters['security_sensors']:
            box_5_1 = BoxLayout(orientation='horizontal', size_hint_y=None, height=50)
            box_5_1.add_widget(Label(text=sensor, font_size=20, color=(3, 1, 1, 1), size_hint=(.7, 1)))
            box_5_1.add_widget(Label(text=str(controller.home_parameters['security_sensors'][sensor]), font_size=20, color=(3, 1, 1, 1),
                                     size_hint=(.3, 1)))
            box_5.add_widget(box_5_1)
        root = RecycleView(size_hint=(1, 0.45))
        root.add_widget(box_5)
        box_1.add_widget(root)
        box_1.add_widget(Label(text=' ', size_hint=(1, .1), font_size=10, color=(2, 1, 1, 2)))
        self.add_widget(box_1)

    def on_leave(self):  # Будет вызвана в момент закрытия экрана
        self.attach_parameters()
        self.clear_widgets()  # очищаем список
        self.manager.transition.direction = 'left'

    def open_popup(self):
        popup = Popup(title='Ошибка', content=Label(text='Есть сработавшие датчики'), size_hint=(0.4, 0.4))
        popup.open()

    def sensors_checker(self, instance, value):
        if value is True:
            if 1 in controller.home_parameters['security_sensors'].values():
                instance.active = False
                self.open_popup()
        else:
            self.attach_parameters()

    def attach_parameters(self, *args):  # обновляет параметры в controller.py
        controller.user_parameters['securityalarm'] = self.switch_1.active


class FireAlarm(Screen):
    def __init__(self, **kw):
        super(FireAlarm, self).__init__(**kw)

    def on_enter(self):  # Будет вызвана в момент открытия экрана
        self.manager.transition.direction = 'right'
        box_1 = BoxLayout(orientation='vertical', spacing=15)
        box_2 = BoxLayout(orientation='horizontal', size_hint=(1, .2))
        box_2.add_widget(Button(text='< Назад', on_press=lambda x: set_screen('menu'), size_hint=(.15, 1)))
        box_2.add_widget(Label(text='Пожарная сигнализация', font_size=40, color=(1, 0.4, 0, 1), size_hint=(.85, 1)))
        box_1.add_widget(box_2)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=2, color=(1, 0.4, 0, 1)))
        box_3 = GridLayout(cols=2, size_hint=(1, .7))
        box_3.add_widget(Label(text='Датчик', font_size=25, color=(1, 0.4, 0, 1), size_hint=(.7, 1)))
        box_3.add_widget(Label(text='Состояние', font_size=25, color=(1, 0.4, 0, 1), size_hint=(.3, 1)))
        for sensor in controller.home_parameters['fire_sensors']:
            box_3.add_widget(Label(text=sensor, font_size=20, color=(1, 0.4, 0, 1), size_hint=(.7, 1)))
            box_3.add_widget(Label(text=str(controller.home_parameters['fire_sensors'][sensor]), font_size=20,
                                   color=(1, 0.4, 0, 1), size_hint=(.3, 1)))
        box_1.add_widget(box_3)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=2, color=(2, 1, 1, 2)))
        self.add_widget(box_1)

    def on_leave(self):  # Будет вызвана в момент закрытия экрана
        self.clear_widgets()  # очищаем список
        self.manager.transition.direction = 'left'


class Video(Screen):
    def __init__(self, **kw):
        self.list_of_cams = [
            ['Камера №1', Switch(active=False, size_hint=(.3, 1)),
             CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=True, on_press=lambda x: self.label_change())],
            ['Камера №2', Switch(active=False, size_hint=(.3, 1)),
             CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=True, on_press=lambda x: self.label_change())],
            ['Камера №3', Switch(active=False, size_hint=(.3, 1)),
             CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=True, on_press=lambda x: self.label_change())]
        ]
        super(Video, self).__init__(**kw)

    def on_enter(self):  # Будет вызвана в момент открытия экрана
        self.manager.transition.direction = 'right'
        box_1 = BoxLayout(orientation='vertical', spacing=15)
        box_2 = BoxLayout(orientation='horizontal', size_hint=(1, .2))
        box_2.add_widget(Button(text='< Назад', on_press=lambda x: set_screen('menu'), size_hint=(.15, 1)))
        box_2.add_widget(Label(text='Видеонаблюдение', font_size=40, color=(0.8, 0.4, 1, 1), size_hint=(.85, 1)))
        box_1.add_widget(box_2)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=2, color=(2, 1, 1, 2)))
        box_3 = GridLayout(cols=3, size_hint=(1, .7))
        box_3.add_widget(Label(text='Камера', font_size=30, color=(0.8, 0.4, 1, 1), size_hint=(.3, 1)))
        box_3.add_widget(Label(text='Вкл/Выкл', font_size=30, color=(0.8, 0.4, 1, 1), size_hint=(.3, 1)))
        box_3.add_widget(Label(text='Автоматически', font_size=30, color=(0.8, 0.4, 1, 1), size_hint=(.4, 1)))
        for cam in self.list_of_cams:
            box_3.add_widget(Label(text=cam[0], font_size=20, color=(0.8, 0.4, 1, 1), size_hint=(.3, 1)))
            cam[1] = Switch(active=cam[1].active, size_hint=(.3, 1))
            box_3.add_widget(cam[1])
            cam[2] = CheckBox(color=(0, 1, 0, 1), size_hint=(.4, 1), active=cam[2].active,
                              on_press=lambda x: self.label_change())
            box_3.add_widget(cam[2])
        box_1.add_widget(box_3)
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=2, color=(2, 1, 1, 2)))
        self.add_widget(box_1)

    def on_leave(self):  # Будет вызвана в момент закрытия экрана
        self.attach_parameters()
        self.clear_widgets()  # очищаем список
        self.manager.transition.direction = 'left'

    def label_change(self):
        self.attach_parameters()
        self.clear_widgets()
        self.on_enter()

    def attach_parameters(self, *args):  # обновляет параметры в controller.py
        methods_of_control_videocamers = [x[2].active for x in self.list_of_cams]
        status_of_videocamers = [x[1].active for x in self.list_of_cams]
        controller.user_parameters['video']['list_of_methods_of_control_videocamers'] = methods_of_control_videocamers
        controller.user_parameters['video']['list_of_status_of_videocamers'] = status_of_videocamers


'''
class Parameters(Screen):
    def __init__(self, **kw):
        super(Parameters, self).__init__(**kw)

    def on_enter(self):  # Будет вызвана в момент открытия экрана
        box_1 = BoxLayout(orientation='vertical', spacing=15)
        box_1.add_widget(Label(text='Параметры', font_size=40, color=(1, 0.4, 0, 1), size_hint=(1, .15)))
        box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=2, color=(1, 0.4, 0, 1)))
        box_2 = GridLayout(cols=2, size_hint_y=None)
        box_2.bind(minimum_height=box_2.setter('height'))
        box_2.add_widget(Label(text='Температура', font_size=20, color=(1, 0.4, 0, 1), size_hint=(.5, .15)))
        box_2.add_widget(Label(text='16.0', font_size=20, color=(1, 0.4, 0, 1), size_hint=(.5, .15)))
        box_2.add_widget(Label(text='Влажность', font_size=20, color=(1, 0.4, 0, 1), size_hint=(.5, .15)))
        box_2.add_widget(Label(text='32.1', font_size=20, color=(1, 0.4, 0, 1), size_hint=(.5, .15)))




        root = RecycleView(size_hint=(1, 0.8))
        root.add_widget(box_2)
        box_1.add_widget(root)

        #box_3 = GridLayout(cols=2, size_hint=(1, .7))
        #box_3.add_widget(Label(text='Датчик', font_size=25, color=(1, 0.4, 0, 1), size_hint=(.7, 1)))
        #box_3.add_widget(Label(text='Состояние', font_size=25, color=(1, 0.4, 0, 1), size_hint=(.3, 1)))
        #for sensor in self.dict_of_sensors:
        #    box_3.add_widget(Label(text=sensor, font_size=20, color=(1, 0.4, 0, 1), size_hint=(.7, 1)))
        #    box_3.add_widget(Label(text=str(self.dict_of_sensors[sensor]), font_size=20, color=(1, 0.4, 0, 1),
        #                           size_hint=(.3, 1)))
        #box_1.add_widget(box_3)
        #box_1.add_widget(Label(text=' ', size_hint=(1, .05), font_size=2, color=(2, 1, 1, 2)))
        #self.add_widget(box_1)

    def on_leave(self):  # Будет вызвана в момент закрытия экрана
        self.clear_widgets()  # очищаем список
        self.manager.transition.direction = 'left'
'''


def open_popup_alarm(*messages):
    popup = Popup(title='Внимание', content=Label(text=messages[0]), size_hint=(0.4, 0.4))
    popup.open()


def set_screen(name_screen):
    sm.current = name_screen


sm = ScreenManager()
sm.add_widget(HomeSystems(name='menu'))
sm.add_widget(Heater(name='heater'))
sm.add_widget(OutLightning(name='out_lightning'))
sm.add_widget(HomeDevices(name='home_devices'))
sm.add_widget(SecurityAlarm(name='security_alarm'))
sm.add_widget(FireAlarm(name='fire_alarm'))
sm.add_widget(Video(name='video'))


class SmartHome(App):
    def build(self):
        return sm


if __name__ == '__main__':
    SmartHome().run()
