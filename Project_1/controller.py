"""This module used for control house"""
import get_data
import sent_data
#import my_training_app
#import time
#from threading import Thread
from datetime import datetime


# follow parameters set by default values:
user_parameters = {'heater': {'desired_temperature': '18.0', 'manual_control': False, 'on_off_heater': False},
                   'outlightning': {'method_of_control': 0, 'time_to_on': '21:00', 'time_to_off': '06:00',
                                    'on_off_lightning': False},
                   'homedevices': {'status_of_home_devices': [False, False, False, False, False, False, False, False],
                                   'time_to_on_of_home_devices': ['00:00', '00:00', '00:00', '00:00', '00:00', '00:00',
                                                                  '00:00', '00:00'],
                                   'time_to_off_of_home_devices': ['00:00', '00:00', '00:00', '00:00', '00:00', '00:00',
                                                                   '00:00', '00:00']},
                   'securityalarm': False,
                   'firealarm': True,
                   'video': {'list_of_methods_of_control_videocamers': [True, True, True],
                             'list_of_status_of_videocamers': [False, False, False]}}
home_parameters = {'temp': 17.0,
                   'lightning_sensor': True,
                   'security_sensors': {'Датчик двери №1': 0, 'Датчик двери №2': 0, 'Датчик двери №3': 0,
                                        'Датчик движения №1': 0, 'Датчик движения №2': 0, 'Датчик движения №3': 0,
                                        'Датчик шума №1': 0, 'Датчик шума №2': 0},
                   'fire_sensors': {'Датчик задымления №1': 0, 'Датчик задымления №2': 0, 'Датчик задымления №3': 0}}


#my_training_app.SmartHome().run()

def control_temperature():
    """control of the temperature in the house"""
    print('controller-temperature')
    home_parameters['temp'] = get_data.get_temperature()
    if user_parameters['heater']['manual_control'] is True:
        if user_parameters['heater']['on_off_heater'] is True:
            print('heater_contactorON')
            sent_data.contactor('192.111.1.0', '1')
        else:
            sent_data.contactor('192.111.1.0', '0')
            print('heater_contactorOFF')
    else:
        if home_parameters['temp'] < float(user_parameters['heater']['desired_temperature']):
            sent_data.contactor('192.111.1.0', '1')
            print('heater_contactorON')
        else:
            sent_data.contactor('192.111.1.0', '0')
            print('heater_contactorOFF')
        #time.sleep(5)


def control_outlightning():
    """control of the outlightning"""
    print('controller-outlightning')
    home_parameters['lightning_sensor'] = get_data.get_lightning_sensor()
    if user_parameters['outlightning']['method_of_control'] == 0:
        home_parameters['lightning_sensor'] = get_data.get_lightning_sensor()
        if home_parameters['lightning_sensor'] is False:
            sent_data.contactor('192.111.1.1', '0')
            print('outlightningOFF')
        else:
            sent_data.contactor('192.111.1.1', '1')
            print('outlightningON')
    elif user_parameters['outlightning']['method_of_control'] == 1:
        time_now = datetime.now().strftime('%H:%M')
        time_now_int_in_minutes = int(time_now[:2])*60 + int(time_now[3:5])
        time_to_on_int_in_minutes = int(user_parameters['outlightning']['time_to_on'][:2])*60 +\
                                    int(user_parameters['outlightning']['time_to_on'][3:5])
        time_to_off_int_in_minutes = int(user_parameters['outlightning']['time_to_off'][:2])*60 +\
                                     int(user_parameters['outlightning']['time_to_off'][3:5])
        if time_to_on_int_in_minutes > time_to_off_int_in_minutes:
            time_to_off_int_in_minutes += 1440
        if time_to_on_int_in_minutes <= time_now_int_in_minutes <= time_to_off_int_in_minutes:
            sent_data.contactor('192.111.1.1', '1')
            print('outlightningON')
        else:
            sent_data.contactor('192.111.1.1', '0')
            print('outlightningOFF')
    else:
        if user_parameters['outlightning']['on_off_lightning'] is False:
            sent_data.contactor('192.111.1.1', '0')
            print('outlightningOFF')
        else:
            sent_data.contactor('192.111.1.1', '1')
            print('outlightningON')


def control_homedevices():
    """control of the homedevices"""
    print('controller-homedevices')
    for device in range(8):
        time_now = datetime.now().strftime('%H:%M')
        time_now_int_in_minutes = int(time_now[:2]) * 60 + int(time_now[3:5])
        time_to_on_int_in_minutes = int(user_parameters['homedevices']['time_to_on_of_home_devices'][device][:2]) *\
                                    60 + int(user_parameters['homedevices']['time_to_on_of_home_devices'][device][3:5])
        time_to_off_int_in_minutes = int(user_parameters['homedevices']['time_to_off_of_home_devices'][device][:2]) *\
                                    60 + int(user_parameters['homedevices']['time_to_off_of_home_devices'][device][3:5])
        if time_to_on_int_in_minutes > time_to_off_int_in_minutes:
            time_to_off_int_in_minutes += 1440
        if user_parameters['homedevices']['status_of_home_devices'][device] is True or time_to_on_int_in_minutes <=\
                time_now_int_in_minutes <= time_to_off_int_in_minutes:
            sent_data.contactor(str(f'192.111.{device+2}.1'), '1')
            print(f'device {device} ON')
        else:
            sent_data.contactor(str(f'192.111.{device+2}.1'), '0')
            print(f'device {device} OFF')


def control_securityalarm():
    """control of the securityalarm"""
    print('controller-securityalarm')
    home_parameters['security_sensors'] = get_data.get_security_sensors()
    if user_parameters['securityalarm'] is True and 1 in home_parameters['security_sensors'].values():
        sent_data.contactor('192.111.1.10', '1')
        print('securityalarmON')
    else:
        sent_data.contactor('192.111.1.10', '0')
        print('securityalarmOFF')


def control_firealarm():
    """control of the firealarm"""
    print('controller-firealarm')
    home_parameters['fire_sensors'] = get_data.get_fire_sensors()
    if user_parameters['firealarm'] is True and 1 in home_parameters['fire_sensors'].values():
        sent_data.contactor('192.111.1.10', '1')
        print('firealarmON')
    else:
        sent_data.contactor('192.111.1.10', '0')
        print('firealarmOFF')


def control_video():
    """control of the video"""
    print('controller-video')
    for cam in range(3):
        if user_parameters['video']['list_of_status_of_videocamers'][cam] is True\
                or (user_parameters['video']['list_of_methods_of_control_videocamers'][cam] is True
                    and 1 in home_parameters['security_sensors'].values()):
            # sent_data.video('1')
            print('videoON')
        else:
            # sent_data.video('0')
            print('videoOFF')

#thread1 = Thread(target=control_house)
#thread2 = Thread(target=my_training_app.SmartHome().run)


#thread2.start()
#thread1.start()
#thread2.join()
#thread1.join()


#if __name__ == '__main__':
#    control()
